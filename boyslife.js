const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

// JavaScript Document
// Note: lines that start with two backslashes are comments - not code!

const bePrepared = () => {
    // = = = = = = = declare all the variables = = = = = = = =
    let tempC, myActionText, newText;
    //get the temp (F) from the readline prompt

    rl.question('What is the temperature in Fahrenheit? ', (tempF) => {

        // = = = = = = = convert the temp to Celsius (with only one decimal place)
        tempC = (5 / 9 * (tempF - 32)).toFixed(1);

        // = = = = = = = evaluate the temp (three categories) = = = = = = =
        if (tempF < 60) {
            myActionText = "Take long-johns!";
        }
        if ((tempF >= 60) && (tempF < 75)) {
            myActionText = "Have Fun!";
        }
        if (tempF >= 75) {
            myActionText = "Take Sunscreen!";
        }

        // = = = = = = = build a complete sentence = = = = = = =
        newText = `If the temperature is ${tempF} degrees F (${tempC} degrees C): ${myActionText}`;
        //push the sentence back to the HTML page (using the ID of the markup element: 'myAnswer')
        console.log(newText);
        rl.close();
    });
};

bePrepared();